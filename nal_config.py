#!/usr/bin/python3

'''
 Network Alive Monitor - Configuration File
 
 As this program is not intended for packaging and intended to run
 by sysadmin users the ConfigParser module is not used to create
 a configuration file. Instead a standard python module is used.

 @version    0.3
 @package    network-alive
 @copyright  Copyright (c) 2020 Martin Sauter
 @license    GNU General Public License v3
 @since      Since Release 1.0.0

'''

''' ----------------------------------------------------------------

 =============== User Modifiable Variables ====================

 ---------------------------------------------------------------- '''

# Links or paths to monitor. Add/remove/modify as required
links_mon = [
        ["Internet", "8.8.8.9", 0, 0, "", "OK"], 
        ["Local Router", "192.168.42.1", 0, 0, "", "OK"], 
        ["Local 2", "192.168.42.240", 0, 0, "", "OK"]
]


