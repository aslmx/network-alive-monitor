#!/usr/bin/python3

'''
 Network Alive Monitor

 @version    0.3.3
 @package    network-alive
 @copyright  Copyright (c) 2020 Martin Sauter
 @license    GNU General Public License v3
 @since      Since Release 0.1

'''

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib
from gi.repository.GdkPixbuf import Pixbuf

import logging
import time
import subprocess
import threading
import os

# Import configuratin file with global variables
from nal_config import links_mon


DATE_FORMAT = "%d.%m.%Y, %H:%M:%S"

''' Constants for the links_mon list imported from nal_config.py '''

LABEL      = 0
DOMAIN     = 1
COUNT_OK   = 2
COUNT_FAIL = 3
RTT        = 4
STATUS     = 5

win = None

''' ----------------------------------------------------------------

 ======================== GUI class ==========================

 ---------------------------------------------------------------- '''

class NetworkAliveGui(Gtk.Window):
    
    liststore = None
    
    def __init__(self):
        Gtk.Window.__init__(self)
        #Gtk.Window.set_resizable(self, False)
        self.set_default_size(50,50)
        self.set_title ("Network Alive Monitor")        
        
        self.box = Gtk.VBox(spacing=1)
        self.add(self.box)
        
        ontop_button = Gtk.ToggleButton(label="On Top")
        ontop_button.connect("toggled", self.on_ontop_button_toggled, "1")
        self.box.pack_start(ontop_button, False, False, 0)
        
        self.liststore = Gtk.ListStore(Pixbuf, str)
        iconview = Gtk.IconView.new()
        iconview.set_model(self.liststore)

        # Each liststore entry shall contain an icon (pixbuf) and a text label
        iconview.set_pixbuf_column(0)
        iconview.set_text_column(1)

        # Disable selection of an icon with the mouse
        iconview.set_selection_mode(0)

        # Create an icon and label for each entry in the links_mon (global)     
        for link_mon in links_mon:
            pixbuf = Gtk.IconTheme.get_default().load_icon("user-available", 16, 0)
            self.liststore.append([pixbuf, link_mon[0]])        

        self.box.pack_start (iconview, True, True, 0)
        
        # Create a context menu (right click) that opens anywhere
        # in the window        
        menu = Gtk.Menu()
        
        menuitem = Gtk.MenuItem("Show log file")
        menuitem.connect('button-press-event', self.on_show_log_entry_selected)
        menu.append(menuitem)
        menuitem.show()        
                
        menuitem = Gtk.MenuItem("Reset all counters")
        menuitem.connect('button-press-event', self.on_reset_counters_entry_selected)
        menu.append(menuitem)
        menuitem.show()
        
        # Handler for right click over the window which will make the 
        # context menu appear)
        self.connect_object('button-press-event', self.on_button_click_in_main_window, menu)


    def on_button_click_in_main_window(self, menu_widget, event):
        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
            # Open the menu
            menu_widget.popup(None, None, None, None, event.button, event.time)

            
    def on_reset_counters_entry_selected(self, widget, event):
        # Reset all counters in global variable
        for x in range(len(links_mon)):
            links_mon[x][COUNT_OK] = 0
            links_mon[x][COUNT_FAIL] = 0
        # Update the GUI immediately
        self.set_list_entries()
        
        
    def on_show_log_entry_selected(self, widget, event):
        # Open log file in system editor
        os.system("xdg-open nal.log")

       
    def on_ontop_button_toggled(self, button, name):
        if button.get_active():
            # Set on top
            self.set_keep_above(True)
        else:
            # Remove on top
            self.set_keep_above(False)

    
    def set_list_entries(self):
        
        modi = self.liststore.get_iter_first()
        
        for x in range(len(links_mon)):

            # Set the label with name, # of replies ok, # replies nok, etc.
            self.liststore.set(modi, 1, links_mon[x][LABEL] + "\n" + 
                               "OK: " + str(links_mon[x][COUNT_OK]) + "\n" +
                               "Fail: " + str(links_mon[x][COUNT_FAIL]) + "\n" +
                               "RTT: " + str(links_mon[x][RTT])
                               )

            # Set the icon for the current link
            if links_mon[x][STATUS] == "OK":                
                pixbuf = Gtk.IconTheme.get_default().load_icon("user-available", 16, 0)
            else:
                pixbuf = Gtk.IconTheme.get_default().load_icon("user-busy", 16, 0)
            self.liststore.set(modi, 0, pixbuf)
            
            modi = self.liststore.iter_next(modi)


''' ----------------------------------------------------------------

 =================== non-GUI functions ======================

 ---------------------------------------------------------------- '''

''' 

 Functions to periodically report the current ping results to the GUI class 

'''

def ping_result_reporter():
    '''
     puts win.set_list_entries() into the GLib idle queue once a second 
     so the update can be made in the main thread. This is necessary as 
     GLib is NOT thread save, so it can't be done directly.
    '''
    
    while True:
        time.sleep(1)
        GLib.idle_add(win.set_list_entries)


def start_ping_result_reporter():

    # Run the ping command handler in a second thread so it will run independently
    # from the GUI endless loop and is able to work while the GUI is running in
    # the first thread.
    thread = None
    thread = threading.Thread(target=ping_result_reporter)    
    thread.daemon = True
    thread.start()  

''' 

 Functions to handle individual ping threads 
 
'''

def ping_thread(x):
    '''
    Launch and maintain a separate process for pinging a domain 
    or IP address.
    
    
    Note: There are ping libraries available for Python but they
    all require the process to run as root. As this is not acceptable
    for this purpose, the shell ping command is run in a separate 
    process that can get permissions independently of the Python
    program using this function.   
         
    '''

    # Start ping with -O option to also report missing responses 
    # immediately and -n for numeric output.
    f = subprocess.Popen(['ping', '-O', '-n', links_mon[x][DOMAIN]],
                          stdout=subprocess.PIPE,stderr=subprocess.PIPE)

    # Ignore the first output line
    line = f.stdout.readline()
    
    fail_count = 0
    
    while True:
        
        line = f.stdout.readline()            
        converted_str = line.decode('utf-8')
        
        if line.startswith(b'64 bytes'):
            links_mon[x][STATUS] = 'OK'
            links_mon[x][COUNT_OK] += 1
            
            # Save the round trip time of the current ping for output
            string_parts = converted_str.split("=")            
            links_mon[x][RTT] = string_parts[len(string_parts)-1].strip()
            
            # If there was a failure before, set the count back to 0
            # and make a log entry
            if fail_count > 0:
                logging.info(links_mon[x][LABEL] + ': response received again')
                fail_count = 0
                    
        else:
            links_mon[x][STATUS] = 'NOK'
            links_mon[x][COUNT_FAIL] += 1
            links_mon[x][RTT] = 'NA'
            
            # Note the failure in the log file up to 4 times.
            fail_count += 1
            if fail_count < 5:
                logging.info(links_mon[x][LABEL] + ': no response')
            if fail_count == 5:
                logging.info('...')
                
        # wait a short while so the loop does not go wild if the ping
        # process dies for whatever reason
        time.sleep(0.2)


def start_ping_threads():
    '''
    Iterates over the links_mon list to create ping threads that check
    connectivity. Each thread runs independently so different execution times
    of individual ping threads do not block reading the result from faster
    pings to other destinations

    '''

    # Launch a separate process for each domain, read first line to ignore it   
    for x in range(len(links_mon)):
        thread = None
        thread = threading.Thread(target=ping_thread, args=(x,))    
        thread.daemon = True
        thread.start()    


''' ----------------------------------------------------------------

 ====================== Initialization Code =====================

 ---------------------------------------------------------------- '''

# Open Log file for info such as pings missed, etc.
# Note: path, if included, must be absolute, it can't start at '~/...'  !!!
logging.basicConfig(filename='nal.log',level=logging.INFO, \
                    format='%(asctime)s %(message)s')

logging.info('Network Alive Monitoring started')


# Initialize the main window
win = NetworkAliveGui()
win.connect("destroy", Gtk.main_quit)
win.show_all()

# Start all ping threads
start_ping_threads()

# Start a reporter thread that reports the latest ping results
# to the GUI object
start_ping_result_reporter() 

# Now enter the Gtk loop that keeps the window updated. This function
# is left when the user ends the program by clicking on the window close icon
Gtk.main()

