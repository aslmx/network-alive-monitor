# Network Alive Monitor

This GTK+ GUI program for Linux pings one or more servers given in the config file (`nal_config.py`) once per second and shows if they are reachable or not. If reachable a green icon is displayed. If not reachable, a red icon is displayed instead. In addition the number of successful and failed pings are shown. Values can be reset via the right-click context menu. In addition the round trip time of the latest successful ping command is shown.

## GUI Screenshots

| **Ubuntu** | **Linux Mint (Mint-Y Theme)** |
| ------ | ------ |
| ![](images/gui-1.png) | ![](images/gui-2-mint.png)


## Implementation Notes

Each ping command is run in its own thread and is thus independent from the response times of pings to another destination. OK/fail/RTT info is written to a global list in each thread. Once a second, a GUI update is triggered in another independent thread.

## Log file

Failing pings will be logged into nal.log in the directory from which the program is started

## Installation

Place `nal.py` and `nal.config` in the same directory and run it from there.

### install.sh

Optionally you can run the **install.sh** file, which will update the **nal.desktop** file and put it into your **/usr/share/applications/** folder, so you can run NetworkAliveMonitor from your applications menu

```
chmod +x install.sh
sudo ./install.sh
```

Of course you can manually edit **nal.desktop** and place it where you need it.

**Note:** There is currently no icon for Network Alive Monitor. The install script will point to "nal.png" in the NAL directory for your convenience. You can usually edit a launcher icon with the tools of your desktop environment manually.


## Icon and Program Launcher / Dock integration

If you want a program icon in the Ubuntu/Debian Gnome dock, copy the `nal.desktop` file to `.local/share/applications` and adapt the path to the program and the icon. Find an icon that suits your needs, e.g. in `/var/lib/app-info/icons`.

## Run

From the console and pushing it to the background + making it independent so the shell can be closed:

`nohup nal.py &`

## OS Versions tested

 * Ubuntu 18.04
 * Ubuntu 20.04
 * Linux Mint 20.04
